package br.com.myclub;

import java.util.Scanner;

public class Cadastro {
	Scanner scanner = new Scanner(System.in);
	
	protected String nome;
	protected String cpf;
	protected String email;
	protected String dataNasc;
	protected int cep;
	protected String senha;
	protected String verificaEmail;
	protected String verificaSenha;
	
	
	public Cadastro(String nome, String cpf, String email, String dataNasc, int cep) {
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.dataNasc = dataNasc;
		this.cep = cep;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}
	public int getCep() {
		return cep;
	}
	public void setCep(int cep) {
		this.cep = cep;
	}
	
	public void criarConta() {
		
	}
	
	public void fazerLogin() {
		System.out.println("Crie uma senha: ");
		senha = scanner.nextLine();
		System.out.println("Senha criada com sucesso");
		System.out.println("========== Login ==========");
		System.out.println("Digite o seu email: ");
		verificaEmail = scanner.nextLine();
		System.out.println("Digite a sua senha: ");
		verificaSenha = scanner.nextLine();
		
		
		if(senha.equals(verificaSenha) && email.equals(verificaEmail)) {
			System.out.println("Dados Corretos, Acesso Permitido");
		}
		while(!senha.equals(verificaSenha) && !email.equals(verificaEmail)) {
			System.out.println("Dados Incorretos, digite novamente");
			
			System.out.println("Digite o seu email: ");
			verificaEmail = scanner.nextLine();
			System.out.println("Digite a sua senha: ");
			verificaSenha = scanner.nextLine();
			
			
			if(senha.equals(verificaSenha) && email.equals(verificaEmail)) {
				System.out.println("Dados Corretos, Acesso Permitido");
			}
		}
		
		
	}
	
	public void alterarCadastro() {
		String alteraCampo;
		System.out.println("========== Altera��o de Cadastro ==========");
		System.out.println("Digite o campo que deseja alterar (nome, email): ");
		alteraCampo = scanner.nextLine();
		if (alteraCampo.equals("nome")) {
			System.out.println("Digite o novo nome: ");
			nome = scanner.nextLine();
			System.out.println(nome);
			System.out.println("O seu novo nome �: " + nome);
		}
		if (alteraCampo.equals("email")) {
			System.out.println("Digite o novo email: ");
			email = scanner.nextLine();
			System.out.println("O seu novo nome �: " + email);
		}
		
	}
	
	
}
