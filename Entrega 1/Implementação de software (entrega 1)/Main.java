package br.com.myclub;

import java.util.Scanner;

public class Main {
	
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Seja Bem Vindo ao MyClub");
		System.out.println("1 - Jogador \n2 - Arbitro");
		System.out.println("\nDigite como deseja se cadastrar(numero): ");
		int selec = Integer.parseInt(scanner.nextLine());
		
		if(selec == 1) {
			Jogador jogador = new Jogador(null, null, null, null, selec);
				jogador.criarConta();
				jogador.fazerLogin();
				jogador.alterarCadastro();
			}
			
			
		
		if(selec == 2) {
			Arbitro arbitro = new Arbitro(null, null, null, null, selec);
				arbitro.criarConta();
				arbitro.fazerLogin();
				arbitro.alterarCadastro();
			
		}


	}

}
